#字信息统计
import sys
import jieba
import collections

word_lst = []
char_dict = {}

#exclude_str = "，。！？、;；’‘（）\]\[【】<>《》=：+-*—“”…"
#path='D:\\Desktop\\西游记.txt'
total_char=0
with open(sys.argv[1],"r",encoding = "UTF-8") as fileIn:
    # 添加每一个字到列表中
    for line in fileIn:
        for char in line:
            if '\u4e00' <= char <= '\u9fef':
#            if char not in exclude_str:
#                if char.strip():
                word_lst.append(char)
                total_char+=1
char_counts = collections.Counter(word_lst)       # 字频统计
char_counts_top = char_counts.most_common(len(char_counts))    # 获取前number个最高频的字
tem = 0           #用于统计累计频率
Type_char = len(set(word_lst))
# 用字典统计每个字出现的个数
for TopChar,Frequency in char_counts_top:
    char_dict.setdefault(TopChar,[]).append(str(Frequency))
    char_dict.setdefault(TopChar,[]).append('{:.2%}'.format(int(Frequency)/total_char))
    tem+=int(Frequency)
    char_dict.setdefault(TopChar,[]).append('{:.2%}'.format(tem/total_char))

TTR_char = Type_char/total_char
print('文本汉字数：{}'.format(total_char))
print('文本的字TTR(Type Token Ratio)：{}'.format(TTR_char))
print ('字符\t字频\t频率\t累加频率')
print ('===========================')
for k in char_dict:
    print('{}	{}	{}	{}'.format(k,char_dict[k][0],char_dict[k][1],char_dict[k][2]))