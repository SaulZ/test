#词信息统计
# 导入扩展库
import re                           # 正则表达式库
import jieba                        # 结巴分词
import jieba.posseg                 # 词性获取
import collections                  # 词频统计库
import sys
from jieba import analyse
En2Cn = {
    'a'    : '形容词',
    'ad'   : '形容词',
    'ag'   : '形容词',
    'al'   : '形容词',
    'an'   : '形容词',
    'b'    : '区别词',
    'bl'   : '区别词',
    'c'    : '连词',
    'cc'   : '连词',
    'd'    : '副词',
    'e'    : '叹词',
    'eng'  : '英文',
    'f'    : '方位词',
    'g'    : '语素',
    'h'    : '前缀',
    'i'    : '成语',
    'j'    : '简称略语',
    'k'    : '后缀',
    'l'    : '习用语',
    'm'    : '数词',
    'mq'   : '数量词',
    'n'    : '名词',
    'ng'   : '名词',
    'nl'   : '名词',
    'nr'   : '名词',
    'nr1'  : '名词',
    'nr2'  : '名词',
    'nrf'  : '名词',
    'nrfg' : '名词',    
    'nrj'  : '名词',
    'ns'   : '名词',
    'nsf'  : '名词',
    'nt'   : '名词',
    'nz'   : '名词',
    'o'    : '拟声词',
    'p'    : '介词',
    'pba'  : '介词',
    'pbei' : '介词',
    'q'    : '量词',
    'qt'   : '量词',
    'qv'   : '量词',
    'r'    : '代词',
    'rg'   : '代词',
    'rr'   : '代词',
    'rz'   : '代词',
    'rzs'  : '代词',
    'rzt'  : '代词',
    'rzv'  : '代词',
    'ry'   : '代词',
    'rys'  : '代词',
    'ryt'  : '代词',
    'ryv'  : '代词',
    's'    : '处所词',
    't'    : '时间词',
    'tg'   : '时间词',
    'u'    : '助词',
    'ude1' : '助词',
    'ude2' : '助词',
    'ude3' : '助词',
    'udeng': '助词',
    'udh'  : '助词',
    'uguo' : '助词',
    'ule'  : '助词',
    'ulian': '助词',
    'uls'  : '助词',
    'usuo' : '助词',
    'uyy'  : '助词',
    'uzhe' : '助词',
    'uzhi' : '助词',
    'v'    : '动词',
    'vd'   : '动词',
    'vf'   : '动词',
    'vg'   : '动词',
    'vi'   : '动词',
    'vl'   : '动词',
    'vn'   : '动词',
    'vshi' : '动词',
    'vx'   : '动词',
    'vyou' : '动词',
    'w'    : '标点符号',
    'wb'   : '标点符号',
    'wd'   : '标点符号',
    'wf'   : '标点符号',
    'wj'   : '标点符号',
    'wh'   : '标点符号',
    'wkz'  : '标点符号',
    'wky'  : '标点符号',
    'wm'   : '标点符号',
    'wn'   : '标点符号',
    'wp'   : '标点符号',
    'ws'   : '标点符号',
    'wt'   : '标点符号',
    'ww'   : '标点符号',
    'wyz'  : '标点符号',
    'wyy'  : '标点符号',
    'x'    : '字符串',
    'xu'   : '字符串',
    'xx'   : '字符串',
    'y'    : '语气词',
    'z'    : '状态词',
    'un'   : '未知词',
}
#文本词语信息统计
tfidf = analyse.extract_tags

# 读取文件
#path='D:\\Desktop\\西游记.txt'
with open(sys.argv[1],'r',encoding = 'UTF-8') as fn:  # 打开文件
    string_data = fn.read()                          # 读出整个文件

# 文本预处理
pattern = re.compile(u'\t|\n|\.|-|:|;|\)|\(|\?|"') # 定义正则表达式匹配模式（空格等）
string_data = re.sub(pattern, '', string_data)     # 将符合模式的字符去除
# 动态调整词典

#jieba.suggest_freq('', True)     #True表示该词不能被分割，False表示该词能被分割
# 添加用户词典
#jieba.load_userdict(userdict)

# 文本分词
seg_list_exact = jieba.cut(string_data, cut_all=False, HMM=True)    # 精确模式分词+HMM
object_list = []
# 去除停用词（去掉一些意义不大的词，如标点符号、嗯、啊等）
with open('chinese_stopwords.txt', 'r', encoding='UTF-8') as meaninglessFile:
    stopwords = set(meaninglessFile.read().split('\n'))
stopwords.add(' ')

total_word=0
for word in seg_list_exact:         # 循环读出每个分词
    #if len(word) > 1:
        if word not in stopwords:       # 如果不在去除词库中
            object_list.append(word)    # 分词追加到列表
            total_word+=1
          
Type=len(set(object_list)) 
# 词频统计
word_counts = collections.Counter(object_list)       # 对分词做词频统计
word_counts_top = word_counts.most_common(len(word_counts))  # 获取前10000个最高频的词
if len(word_counts) < 10000:
    number=len(word_counts)
elif len(word_counts) >=10000:
    number = 10000
word_dict={}
gram_dict={'two':0,'three':0,'four':0,'substantive':0}
substantive_l=['名词','形容词','动词']

tem = 0                                                         #用于计算累计频率
for TopWord,Frequency in word_counts_top:                       # 获取词语和词频
    for POS in jieba.posseg.cut(TopWord):                       # 获取词性
        
#         if count == number:
#             break
        if TopWord not in word_dict:
            try:
                word_dict.setdefault(TopWord,[]).append(str(Frequency))
                word_dict.setdefault(TopWord,[]).append('{:.2%}'.format(int(Frequency)/total_word))
                tem+=int(Frequency)
                word_dict.setdefault(TopWord,[]).append('{:.2%}'.format(tem/total_word))
                if len(TopWord)==2:
                    gram_dict['two']+=1
                elif len(TopWord)==3:
                    gram_dict['three']+=1
                elif len(TopWord)==4:
                    gram_dict['four']+=1
                word_dict.setdefault(TopWord,[]).append(list(En2Cn.values())[list(En2Cn.keys()).index(POS.flag)])
                if list(En2Cn.values())[list(En2Cn.keys()).index(POS.flag)] in substantive_l:
                    gram_dict['substantive']+=1
            except ValueError:
                word_dict.setdefault(TopWord,[]).append('其他')
# print(tem)
# print(total_word)
TTR_word=Type/total_word
# 基于TF-IDF算法进行关键词抽取

jieba.analyse.set_stop_words('chinese_stopwords.txt')
keywords = tfidf(string_data,topK=20, withWeight=False, allowPOS=())

# 输出抽取出的关键词

print('实词数：{}'.format(gram_dict['substantive']))
print('二字词数：{}'.format(gram_dict['two']))
print('三字词数：{}'.format(gram_dict['three']))
print('四字词数：{}'.format(gram_dict['four']))
print('文本的词语TTR(Type Token Ratio):{}'.format(TTR_word))
print('关键词：'+'、'.join(keywords))

print ('  前{}高频词\n词语\t词频\t频率\t累加频率\t词性'.format(number))
print ('———————————————————————')
num=0
for k in word_dict:
    print('{}   {}      {}        {}           {}'.format(k,word_dict[k][0],word_dict[k][1],word_dict[k][2],word_dict[k][3]))
    num+=1
    if num==number:
        break

